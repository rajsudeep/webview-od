//
//  ViewController.swift
//  Web View
//
//  Created by Sudeep Raj on 11/4/20.
//

import UIKit
import WebKit

struct Response: Encodable {
    var requestId: String? = ""
    var data: String? = ""
}
struct CardResponse: Encodable {
    var requestId: String? = ""
    var data: Card
}
struct Card: Encodable {
    var firstName: String = "Bharghavan"
    var lastName: String = "Vaduvur"
    var cardNumber: String = "4120 4695 2302 6643"
    var expMonth: String = "02"
    var expYear: String = "2023"
    var zipCode: String = "32112"
}

class ViewController: UIViewController, WKUIDelegate {
   var webView: WKWebView!
   override func viewDidLoad() {
      super.viewDidLoad()
      let myURL = URL(string:"http://54.161.77.239:3000")
      let myRequest = URLRequest(url: myURL!)
      webView.load(myRequest)
   }
   override func loadView() {
      let webConfiguration = WKWebViewConfiguration()
      webView = WKWebView(frame: .zero, configuration: webConfiguration)
      webView.uiDelegate = self
      view = webView
   }
}

//class ViewController: UIViewController, WKScriptMessageHandler {
//    var cache: String?
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        var webView: WKWebView!
//        let config = WKWebViewConfiguration()
//        let contentController = WKUserContentController()
//        contentController.add(self, name: "store")
//        contentController.add(self, name: "retrieve")
//        contentController.add(self, name: "resetPin")
//        contentController.add(self, name: "getCardInfo")
//        config.userContentController = contentController
//        webView = WKWebView(frame: view.bounds, configuration: config)
//        view.addSubview(webView)
//
//        if let url = URL(string: "https://webview-controls.surge.sh/components") {
//            webView.load(URLRequest(url: url))
//        }
//    }
//    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
//        if message.name == "store" {
//            print("Storing \(message.body)")
//            cache = message.body as? String
//        }
//        if message.name == "retrieve" {
//            if let messageBody = message.body as? [String: Any], let requestId = messageBody["requestId"] as? String {
//                let res = Response(requestId: requestId, data: cache ?? "undefined")
//
//                let encoder = JSONEncoder()
//                let data = try! encoder.encode(res)
//                print(String(data: data, encoding: .utf8)!)
//                message.webView?.evaluateJavaScript("respondToWebView('\(String(data: data, encoding: .utf8)!)')") { (any, error) in
//                    print("Error : \(String(describing: error))")
//                }
//            }
//        }
//
//        if message.name == "resetPin" {
//            if let messageBody = message.body as? [String: Any], let requestId = messageBody["requestId"] as? String, let pin = messageBody["pin"] as? String {
//                print("resetting pin \(pin)")
//                let res = Response(requestId: requestId, data: "Your pin has been resetted")
//
//                let encoder = JSONEncoder()
//                let data = try! encoder.encode(res)
//                print(String(data: data, encoding: .utf8)!)
//                message.webView?.evaluateJavaScript("respondToWebView('\(String(data: data, encoding: .utf8)!)')") { (any, error) in
//                    print("Error : \(String(describing: error))")
//                }
//            }
//        }
//
//        if message.name == "getCardInfo" {
//            if let messageBody = message.body as? [String: Any], let requestId = messageBody["requestId"] as? String {
//                let encoder = JSONEncoder()
//                let res = CardResponse(requestId: requestId, data: Card())
//
//                let data = try! encoder.encode(res)
//                print(String(data: data, encoding: .utf8)!)
//                message.webView?.evaluateJavaScript("respondToWebView('\(String(data: data, encoding: .utf8)!)')") { (any, error) in
//                    print("Error : \(String(describing: error))")
//                }
//            }
//        }
//    }
//}
